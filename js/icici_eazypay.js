$(document).ready(function () {
    $(window).scroll(function () {
        if ($(this).scrollTop() > 30) {
            $('header').addClass('activBG');
        } else {
            $('header').removeClass('activBG');
        }
    });

    $('#hamburger').click(function () {
        $(this).toggleClass('open');
        if ($(this).hasClass('open')) {            
            $('.mobile_menu_container').css('display', 'block');
            $('body').addClass('hideScroll');
            $('.mobile_menu_container').animate({
                opacity: 1,
            }, 'fast', function () {
                /*alert("reached top");*/
            });
        } else {
            $('.mobile_product_container').animate({
                opacity: 0,
            }, 'fast', function () {
                $('.mobile_product_container').css('display', 'none');               
            });
            $('.mobile_menu_container').animate({
                opacity: 0
            }, 'fast', function () {
                $('body').removeClass('hideScroll');
                $('.mobile_menu_container').css('display', 'none');
            });            
        }
    });

    $('.mobileProduct').click(function () {
        $('.mobile_product_container').css('display', 'block');
        $('.mobile_product_container').animate({
            opacity: 1,
        }, 'fast', function () {
            /*alert("reached top");*/
        });
        return false;
    });

    $('.mobileProductBack').click(function () {       
        $('.mobile_product_container').animate({
            opacity: 0,
        }, 'fast', function () {
            $('.mobile_product_container').css('display', 'none');
        });
        return false;
    });    
});